//select the canvas element in html document
var canvas = document.getElementById("myCanvas");
//set the canvas to screen width & height
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

//Create a Drawing Object
var ctx = canvas.getContext('2d');

//Rectangle
//Set the fill style of the drawing object
ctx.fillStyle = "rgba(255, 0, 0, 0.4)";
//fillRect(x,y,width,height) method draws a rectangle
ctx.fillRect(200, 100, 100, 100); //rectangle 1

ctx.fillStyle = "rgba(0, 255, 0, 0.4)";
ctx.fillRect(300, 200, 100, 100); //rectangle 2

ctx.fillStyle = "rgba(0, 0, 255, 0.4)";
ctx.fillRect(400, 300, 100, 100); //rectangle 3

//Line
/*
To draw a straight line on a canvas, use the following methods:
   * moveTo(x, y) - defines the starting point of the line
   * lineTo(x, y) - defines the ending point of the line
   * Then use the stroke() method to actually draw the line
*/
ctx.beginPath();
ctx.moveTo(0,200);
ctx.lineTo(300,200);
ctx.lineTo(300,0);
ctx.lineTo(0,200);
ctx.strokeStyle = "#ffa24b";
ctx.stroke();

//Arc or Circle
/*
To draw a circle on a canvas, use the following methods:
    * beginPath() - begins a path
    * arc(x, y, r, startangle:radians, endangle:radians, drawCounterClockwise: Bool (false)) - creates an arc/curve.
    * To create a circle with arc(): Set start angle to 0 and end angle to 2*Math.PI. The x and y parameters define the x- and y-coordinates of the center of the circle. The r parameter defines the radius of the circle.
*/
ctx.beginPath();
ctx.arc(300, 300, 50, 0, 2 * Math.PI , false);
ctx.strokeStyle = "blue";
ctx.stroke();
//Creating multiple circles using for-loop
/*
for (var i = 0; i < 3; i++){
    var x = Math.random() * window.innerWidth;
    var y = Math.random() * window.innerHeight;
    ctx.beginPath();
    ctx.arc(x, y, 50, 0, 2 * Math.PI , false);
    ctx.strokeStyle = "red";
    ctx.stroke();
}*/
