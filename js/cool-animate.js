/* Created by HP on 6/16/2018.*/
/* Author: Mainul Hasan.*/
//select the canvas element in html document
var canvas = document.getElementById("myCanvas");
//set the canvas to screen width & height
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

//Create a Drawing Object
var c = canvas.getContext('2d');

//Create an object to hold mouse coordinates
var mouse = {
    x: undefined,
    y: undefined
};
var maxRadius = 40;

//Add colors for circles
var colorArray = [
    '#47466D',
    '#8EA0D5',
    '#58648A',
    '#BBB5DA',
    '#F5EFE1'
]
//Add event listener
window.addEventListener('mousemove',function (event) {
    mouse.x = event.x;
    mouse.y = event.y;
})
window.addEventListener('resize',function () {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    initCircle();
})

//Circle object constructor
/*
 In JS, the thing called 'this' is the object that "owns" the code. The value of this, when used in an object, is the object itself. In a constructor function this does not have a value. It is a substitute for the new object. The value of this will become the new object when a new object is created. Note that this is not a variable. It is a keyword. You cannot change the value of this.
 */
function Circle(x, y, dx, dy, radius) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.minRadius = radius;
    this.color = colorArray[Math.floor(Math.random() * colorArray.length)];

    this.draw  = function () {
        //draw the circle
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        c.fillStyle = this.color;
        c.fill();
    }

    this.update = function () {
        //make the circle bounce off edges
        if (this.x + radius > innerWidth || this.x - radius < 0) {
            this.dx = -this.dx;
        }
        if (this.y + this.radius > innerHeight || this.y - this.radius < 0){
            this.dy = -this.dy;
        }
        this.x += this.dx;
        this.y += this.dy;
        //Interactivity
        //radius of circles within 50px radius from mouse pointer grow
        if (mouse.x - this.x <50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50){
            if ( this.radius < maxRadius){
                this.radius += 1;
            }
        } else  if ( this.radius > this.minRadius){
            this.radius -= 1;
        }


        //finally draw the circle
        this.draw();
    }
} //Circle object ends

//create an array to store multiple circles
var circleNumber = 800;
var circleArray = [];
function initCircle() {
    circleArray = [];
    for (var i = 0; i < circleNumber; i++){
        var radius = Math.random() * 3 + 1; //circle radius varies from 1 to 4
        /*
         Imp: If we spawn our circle between 0 and innerWidth or height of the screen, the circle will get caught up at the edges and result in an undesired effect. To solve this, we produce the circle between the value of the circle's radius and innerWidth or height minus the diameter of the circle
         */
        var x = Math.random() * (window.innerWidth - radius * 2) + radius; //starting point of our circle in x-axis
        var y = Math.random() * (window.innerHeight - radius * 2) + radius; //starting point of our circle in y-axis
        var dx = (Math.random() - 0.5) * 2; // the velocity at which the circle moves along the x-axis
        var dy = (Math.random() - 0.5) * 2; // the velocity at which the circle moves along the y-axis
        circleArray.push(new Circle(x, y, dx, dy, radius)); //add the new circle to the array
    }
}
//Animation
function animate() {
    requestAnimationFrame(animate);
    /*
     context.clearRect(x,y,width,height); clears a rectangular area of (width x height) at coordinates (x,y)
     * x = The x-coordinate of the upper-left corner of the rectangle to clear
     * y = The y-coordinate of the upper-left corner of the rectangle to clear
     * width = The width of the rectangle to clear, in pixels
     * height = The height of the rectangle to clear, in pixels
     */
    c.clearRect(0, 0, window.innerWidth, window.innerHeight); // Clear a rectangle within a given rectangle
    for (var i = 0; i < circleArray.length; i++){
        circleArray[i].update();
    }
}
initCircle();
animate();